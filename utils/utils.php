<?php

function check_market_empty($array)
{

  if (count($array['market']) > 0)
    return TRUE;
  else
    return FALSE;
}

function get_region_order_array($region_order)
{
  $temp_order = json_decode($region_order);
  
  $region_order = array();
  foreach ($temp_order as $item) {
    $temp = array();
    foreach($item as $item2) {
      $temp[$item2] = array();
    }
    $region_order[] = $temp;
  }

  return $region_order;
}
