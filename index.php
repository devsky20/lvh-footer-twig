<?php
require 'vendor/autoload.php';

use Kint\Kint;
error_reporting(E_ALL); 
ini_set('display_errors', ($showErrors) ? 1 : 0);

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

//load data
require_once 'constants/constants.php';
include_once 'constants/region.php';
include_once 'constants/market.php';
include_once 'constants/cms.php';

// compile data
$data = json_decode($cms['data'][REGION_ORDER]['description']);
foreach ($data as &$regions){
    foreach ($regions as &$regionItem){
        $regionId=$region["indexes"]["code"][$regionItem];
        $regionItem=$region["data"][$regionId];
        $regionItem["markets"]=[];
        
        foreach ($market["indexes"]["parent_id"][$regionId] as $marketId){
            if ($market["data"][$marketId]["active_rent"]){
                 $regionItem["markets"][]=$market["data"][$marketId];
            }         
        }    
    }    
}

if (isset($_REQUEST['data'])){
   echo json_encode($data);die; 
} else {
   echo $twig->render('footer.twig',['data' => $data]); 
}