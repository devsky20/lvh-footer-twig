<?php

$cms = array(
  'data' =>
  array(
    3628 =>
    array(
      'id' => '3628',
      'label' => '_Constants',
      'code' => 'Constants',
      'parent_id' => '0',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '',
    ),
    3629 =>
    array(
      'id' => '3629',
      'label' => 'Address Street',
      'code' => 'AddressStreet',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '505 Fifth Avenue, 15th Floor',
    ),
    3630 =>
    array(
      'id' => '3630',
      'label' => 'Address City State Country',
      'code' => 'CityStateCountry',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'New York, NY 10017, USA',
    ),
    3631 =>
    array(
      'id' => '3631',
      'label' => 'Website',
      'code' => 'Website',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'URL',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'http://lvhglobal.com',
    ),
    3633 =>
    array(
      'id' => '3633',
      'label' => 'Phone Main',
      'code' => 'PhoneMain',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '1-212-244-4001',
    ),
    3634 =>
    array(
      'id' => '3634',
      'label' => 'Email Booking',
      'code' => 'EmailBooking',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'booking@lvhglobal.com',
    ),
    3635 =>
    array(
      'id' => '3635',
      'label' => 'Social Facebook',
      'code' => 'SocialFacebook',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'URL',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'https://www.facebook.com/lvhglobal',
    ),
    3636 =>
    array(
      'id' => '3636',
      'label' => 'Social Instagram',
      'code' => 'SocialInstagram',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'URL',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'https://www.instagram.com/lvhglobalofficial',
    ),
    3637 =>
    array(
      'id' => '3637',
      'label' => 'Social LinkedIn',
      'code' => 'SocialLinkedIn',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'URL',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'https://www.linkedin.com/company/lvh-global',
    ),
    3638 =>
    array(
      'id' => '3638',
      'label' => 'Social Youtube',
      'code' => 'SocialYoutube',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'URL',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'https://www.youtube.com/channel/UCL24i6WFP8FNiEYJvylsWiw',
    ),
    3671 =>
    array(
      'id' => '3671',
      'label' => 'Video Company',
      'code' => 'VideoCompany',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '99Bmq6ye',
    ),
    3971 =>
    array(
      'id' => '3971',
      'label' => 'Region Order',
      'code' => 'RegionOrder',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '[
	["USA","CAR"],
	["SOA","AAA"],
	["EUR"]
]',
    ),
    3207 =>
    array(
      'id' => '3207',
      'label' => 'Our Services',
      'code' => 'Services',
      'parent_id' => '0',
      'sort' => '0',
      'value1' => 'Page',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '
',
    ),
    3208 =>
    array(
      'id' => '3208',
      'label' => 'Image Homes',
      'code' => 'ImageHomeIntro',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'FinestHomes.jpeg',
    ),
    3209 =>
    array(
      'id' => '3209',
      'label' => 'The Finest Homes',
      'code' => 'BlobHomesIntro',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Luxury villas & homes from around the world.  Luxury villas & homes from around the world. Luxury villas & homes from around the world. Luxury villas & homes from around the world. Luxury villas & homes from around the world. Luxury villas & homes from around the world.Luxury villas & homes from around the world.Luxury villas & homes from around the world
',
    ),
    3210 =>
    array(
      'id' => '3210',
      'label' => 'Homeowners Services List',
      'code' => 'BlobHomesDetail',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'JSON',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' =>
      array(
        0 => 'We select the finest homes in our favorite destinations around the globe, each home meeting our high standards',
        1 => 'We transform all our homes to your very own private resort',
        2 => 'We are the only company to inspect and visit every home in person before you arrive',
        3 => 'We are always looking for the most prestigious homes in prime locations',
        4 => 'We have the best home selection worldwide',
        5 => 'We offer varied array of exclusive and unique homes',
      ),
    ),
    3211 =>
    array(
      'id' => '3211',
      'label' => 'Images Services',
      'code' => 'ImageServicesIntro',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'About_Us.png',
    ),
    3212 =>
    array(
      'id' => '3212',
      'label' => 'Services',
      'code' => 'BlobServicesIntro',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => NULL,
    ),
    3213 =>
    array(
      'id' => '3213',
      'label' => 'Image Homeowners',
      'code' => 'BlobHomeownerIntro',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Homeowners.png',
    ),
    3214 =>
    array(
      'id' => '3214',
      'label' => 'Homeowners',
      'code' => 'BlobHomeownerIntro',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Listing your property with us will guarantee an increase in your property income and will maximize property exposure. 

With our exclusive network of partners, concierge services, and travel agencies from all over the globe, we will promote your home in online travel magazines, internet blogs, and search engines specifically for your market area.                                                                                        
',
    ),
    3215 =>
    array(
      'id' => '3215',
      'label' => 'Image Partner',
      'code' => 'ImagePartnerIntro',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Partners.png',
    ),
    3216 =>
    array(
      'id' => '3216',
      'label' => 'Partners',
      'code' => 'BlobPartnerIntro',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'This exclusive program has been designed to enable networking between agents, agencies, and influencers from around the globe. we provide direct access to the most luxurious vacation homes in over 40 major destinations around the world. as a partner, you can list all your properties and your clients and book any properties listed with us.                                                                                                                                                        
',
    ),
    3217 =>
    array(
      'id' => '3217',
      'label' => 'Image Affiliates',
      'code' => 'ImageAffiliatesIntro',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Affiliates.png',
    ),
    3218 =>
    array(
      'id' => '3218',
      'label' => 'Affiliates ',
      'code' => 'BlobAffiliatesIntro',
      'parent_id' => '3207',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Hundreds of reputable agencies around the world
',
    ),
    3219 =>
    array(
      'id' => '3219',
      'label' => 'Destination USP',
      'code' => 'BlobSellingPoints',
      'parent_id' => '3258',
      'sort' => '0',
      'value1' => 'JSON',
      'value2' => 'null',
      'value3' => 'null',
      'value4' => '',
      'description' =>
      array(
        0 => 'Key Selling Point 1',
        1 => 'Key Selling Point 2',
        2 => 'Key Selling Point 3',
      ),
    ),
    3220 =>
    array(
      'id' => '3220',
      'label' => 'Homeowners',
      'code' => 'ServicesHomeowner',
      'parent_id' => '0',
      'sort' => '0',
      'value1' => 'Page',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '
',
    ),
    3221 =>
    array(
      'id' => '3221',
      'label' => 'Image Homeowner',
      'code' => 'ImageHomeowner',
      'parent_id' => '3220',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Homeowners.png',
    ),
    3222 =>
    array(
      'id' => '3222',
      'label' => 'Homeowners',
      'code' => 'BlobHomeownerIntro',
      'parent_id' => '3220',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Listing your property with us will guarantee an increase in your property income and will maximize property exposure. With our exclusive network of partners, concierge services, and travel agencies from all over the globe, we will promote your home in online travel magazines, internet blogs, and search engines specifically for your market area."

Our program is designed to remove hassle for our homeowners by managing the entire client reservation process from the time our client plans a trip to the moment they depart. 

Based on close relationships with owners and agencies, we can help sell or acquire unique luxury properties around the world. our dedicated team will help manage your property to increase your income without the hassle of having to self-manage your property. lvh global will handle all inquiries and bookings for your property. ?                                                                                                                                                												
',
    ),
    3224 =>
    array(
      'id' => '3224',
      'label' => 'Benefits To You',
      'code' => 'BlobHomeownerBenefits',
      'parent_id' => '3220',
      'sort' => '0',
      'value1' => 'JSON',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' =>
      array(
        0 => 'Lorem ipsum is simply lorem ipsum dolor sit amet non consequeter lorem ispum',
        1 => 'Lorem ipsum is simply lorem ipsum dolor sit amet non consequeter lorem ispum',
        2 => 'Lorem ipsum is simply lorem ipsum dolor sit amet non consequeter lorem ispum',
        3 => 'Lorem ipsum is simply lorem ipsum dolor sit amet non consequeter lorem ispum',
        4 => 'Lorem ipsum is simply lorem ipsum dolor sit amet non consequeter lorem ispum',
      ),
    ),
    3225 =>
    array(
      'id' => '3225',
      'label' => 'Homeowners CTA',
      'code' => 'BlobHomeownerCTA',
      'parent_id' => '3220',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'LVH Global represents many of the world\'s finest luxury homes and villas. We believe in relationship building. We view every transaction as an opportunity to extend our expertise and world-class service to both our guests as well as our homeowners. Contact us if you want to list your home for rent or sale!',
    ),
    3226 =>
    array(
      'id' => '3226',
      'label' => 'Partners',
      'code' => 'ServicesPartner',
      'parent_id' => '0',
      'sort' => '0',
      'value1' => 'Page',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '
',
    ),
    3227 =>
    array(
      'id' => '3227',
      'label' => 'Partners',
      'code' => 'BlobPartnerIntro',
      'parent_id' => '3226',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'This exclusive program has been designed to enable networking between agents, agencies, and influencers from around the globe. we provide direct access to the most luxurious vacation homes in over 40 major destinations around the world. as a partner, you can list all your properties and your clients and book any properties listed with us.																			
',
    ),
    3228 =>
    array(
      'id' => '3228',
      'label' => 'Our Program',
      'code' => 'BlobPartnerProgram',
      'parent_id' => '3226',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Our program is designed to remove hassle for our homeowners by managing the entire client reservation process from the time our client plans a trip to the moment they depart. 
										
',
    ),
    3229 =>
    array(
      'id' => '3229',
      'label' => 'Benefits To You',
      'code' => 'BlobPartnerBenefits',
      'parent_id' => '3226',
      'sort' => '0',
      'value1' => 'JSON',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' =>
      array(
        0 => 'Lorem ipsum is simply lorem ipsum dolor sit amet non consequeter lorem ispum',
        1 => 'Lorem ipsum is simply lorem ipsum dolor sit amet non consequeter lorem ispum',
        2 => 'Lorem ipsum is simply lorem ipsum dolor sit amet non consequeter lorem ispum',
        3 => 'Lorem ipsum is simply lorem ipsum dolor sit amet non consequeter lorem ispum',
        4 => 'Lorem ipsum is simply lorem ipsum dolor sit amet non consequeter lorem ispum',
      ),
    ),
    3230 =>
    array(
      'id' => '3230',
      'label' => 'Events',
      'code' => 'ServiceEvents',
      'parent_id' => '0',
      'sort' => '0',
      'value1' => 'Page',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '
',
    ),
    3231 =>
    array(
      'id' => '3231',
      'label' => 'Image Events',
      'code' => 'ImageEvents',
      'parent_id' => '3230',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Global_Events.png',
    ),
    3232 =>
    array(
      'id' => '3232',
      'label' => 'Global Events',
      'code' => 'BlobEventsIntro',
      'parent_id' => '3230',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Our international event division specializes in world-class luxury and bespoke services.  We are known for our elegant, innovative, and luxurious receptions.  Due to very high demand from our clients, LVH Global has launched its own event division to host a diverse range of events, specializing in catering to all guests, particularly those with a taste for high-end luxury. the lvh global team of professionals has the experience to turn any event into a world-class experience that will be cherished and remembered forever.                                                                                                                                                               
',
    ),
    3233 =>
    array(
      'id' => '3233',
      'label' => 'Event Services List',
      'code' => 'BlobEventsSpecialities',
      'parent_id' => '3230',
      'sort' => '0',
      'value1' => 'JSON',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' =>
      array(
        0 => 'Butler',
        1 => 'Security',
        2 => 'Chef',
        3 => 'Servers',
        4 => 'Nanny',
        5 => 'Maid service',
        6 => 'Trainer',
        7 => 'Personal shopper',
        8 => 'Chauffeured car',
        9 => 'Security',
      ),
    ),
    3234 =>
    array(
      'id' => '3234',
      'label' => 'Image Events Gallery',
      'code' => 'ImageEventsGallery',
      'parent_id' => '3230',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Gallery.png',
    ),
    3236 =>
    array(
      'id' => '3236',
      'label' => 'Events CTA',
      'code' => 'BlobEventsCTA',
      'parent_id' => '3230',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'LVH Global represents many of the world\'s finest luxury homes and villas. We believe in relationship building. We view every transaction as an opportunity to extend our expertise and world-class service to both our guests. Contact us here if you would like to speak to us about your new upcoming event! ',
    ),
    3237 =>
    array(
      'id' => '3237',
      'label' => 'Concierge',
      'code' => 'ServiceConcierge',
      'parent_id' => '0',
      'sort' => '0',
      'value1' => 'Page',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '
',
    ),
    3238 =>
    array(
      'id' => '3238',
      'label' => 'Image Concierge',
      'code' => 'ImageConcierge',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Concierge_Services.png',
    ),
    3239 =>
    array(
      'id' => '3239',
      'label' => 'Concierge Services',
      'code' => 'BlogConciergeIntro',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Our concierge team removes the burden of vacation planning, freeing you to experience the next adventure of a lifetime in the most luxurious global destinations. With ease and expertise, our concierge team combines local knowledge and exceptional personal assistance to ensure your perfect vacation. We provide phenomenal services tailored for your needs including transportation arrangements, spa reservations, private chefs and catering, restaurant and nightclub reservations, concert and theatre tickets, and special event planning, among other special requests.
',
    ),
    3240 =>
    array(
      'id' => '3240',
      'label' => 'Image Concierge Experience',
      'code' => 'ImageConciergeExperience',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'The_Finest_Travel_Experience.png',
    ),
    3241 =>
    array(
      'id' => '3241',
      'label' => 'Entertainment',
      'code' => 'BlobConciergeExperience',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'We craft the finest travel experiences by offering entertainment through the arrangement and procurement.
',
    ),
    3242 =>
    array(
      'id' => '3242',
      'label' => 'Concierge Experience List',
      'code' => 'BlobConciergeExperienceList',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'JSON',
      'value2' => 'null',
      'value3' => 'null',
      'value4' => '',
      'description' =>
      array(
        0 => 'Show and concert tickets',
        1 => 'In-house entertainment',
        2 => 'Local tourist attractions',
        3 => 'Restaurant Reservations',
        4 => 'VIP club tables',
      ),
    ),
    3243 =>
    array(
      'id' => '3243',
      'label' => 'Image Concierge Sports',
      'code' => 'ImageConciergeSports',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Sports_Activities.png',
    ),
    3244 =>
    array(
      'id' => '3244',
      'label' => 'Sports Activities',
      'code' => 'BlobConciergeSports',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'No excursion is complete without a full itinerary of activities and attractions
',
    ),
    3245 =>
    array(
      'id' => '3245',
      'label' => 'Concierge Sports List',
      'code' => 'BlobConciergeSportsList',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'JSON',
      'value2' => 'null',
      'value3' => 'null',
      'value4' => '',
      'description' =>
      array(
        0 => 'Golf',
        1 => 'Tennis',
        2 => 'Beach volleyball',
        3 => 'Water sports',
        4 => 'Biking',
        5 => 'Horse backriding',
        6 => 'Any other sport you desire',
      ),
    ),
    3246 =>
    array(
      'id' => '3246',
      'label' => 'Image Concierge Spa',
      'code' => 'ImageConciergeSpa',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Spa.png',
    ),
    3247 =>
    array(
      'id' => '3247',
      'label' => 'Spa Service In & Out',
      'code' => 'BlobConciergeSpa',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'We have selected the best spa professionals from any destination to provide you with the best and most relaxing spa experience
',
    ),
    3248 =>
    array(
      'id' => '3248',
      'label' => 'Concierge Spa List',
      'code' => 'BlobConciergeSpaList',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'JSON',
      'value2' => 'null',
      'value3' => 'null',
      'value4' => '',
      'description' =>
      array(
        0 => 'Massage',
        1 => 'Facials',
        2 => 'Manicures',
        3 => 'Hairdressing',
      ),
    ),
    3249 =>
    array(
      'id' => '3249',
      'label' => 'Image Concierge Staff',
      'code' => 'ImageConciergeStaff',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Additional_Staff.png',
    ),
    3250 =>
    array(
      'id' => '3250',
      'label' => 'Additional Staff',
      'code' => 'BlobConciergeStaff',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
',
    ),
    3251 =>
    array(
      'id' => '3251',
      'label' => 'Concierge Staff List',
      'code' => 'BlobConciergeStaffList',
      'parent_id' => '3237',
      'sort' => '0',
      'value1' => 'JSON',
      'value2' => 'null',
      'value3' => 'null',
      'value4' => '',
      'description' =>
      array(
        0 => 'Butler',
        1 => 'Security',
        2 => 'Chef',
        3 => 'Servers',
        4 => 'Nanny',
        5 => 'Maid Service',
        6 => 'Trainer',
        7 => 'Personal Shopper',
        8 => 'Chauffeured Car',
        9 => 'Security',
      ),
    ),
    3255 =>
    array(
      'id' => '3255',
      'label' => 'Experiences',
      'code' => 'DestinationExperiences',
      'parent_id' => '0',
      'sort' => '0',
      'value1' => 'Page',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '
',
    ),
    3256 =>
    array(
      'id' => '3256',
      'label' => 'Image Experiences',
      'code' => 'ImageExperiences',
      'parent_id' => '3255',
      'sort' => '0',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'About_Us.png',
    ),
    3257 =>
    array(
      'id' => '3257',
      'label' => 'Experiences',
      'code' => 'BlobExperiencesIntro',
      'parent_id' => '3255',
      'sort' => '0',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'LVH goes above and beyond to personalize your vacation and to create the dream vacation experience you deserve. 

We work with each client to create a unique and customized experience for you and your guests. 					',
    ),
    3258 =>
    array(
      'id' => '3258',
      'label' => 'Destination',
      'code' => 'Destination',
      'parent_id' => '0',
      'sort' => '0',
      'value1' => 'Page',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '
',
    ),
    3821 =>
    array(
      'id' => '3821',
      'label' => 'Tagline',
      'code' => 'CompanyTagline',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => '',
      'description' => 'Luxury Vacation Homes',
    ),
    3822 =>
    array(
      'id' => '3822',
      'label' => 'Company Name',
      'code' => 'CompanyName',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'LVH Global',
    ),
    3823 =>
    array(
      'id' => '3823',
      'label' => 'Page Title',
      'code' => 'PageTitlePreamble',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => '',
      'description' => 'Luxury Villa Rentals & Vacation Homes',
    ),
    3824 =>
    array(
      'id' => '3824',
      'label' => 'Company Copyright',
      'code' => 'CompanyCopyright',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => NULL,
    ),
    3825 =>
    array(
      'id' => '3825',
      'label' => 'Company Privacy Policy',
      'code' => 'CompanyPrivacyPolicy',
      'parent_id' => '3628',
      'sort' => '0',
      'value1' => 'HTML',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '<iframe width="100%" style="height: 95vh" src="https://app.termly.io/document/privacy-policy/37003208-38e5-4409-8284-7c3f8fadd8da" frameborder="0" allowfullscreen>
<p>Your browser does not support iframes.</p>
</iframe>',
    ),
    3200 =>
    array(
      'id' => '3200',
      'label' => 'Home',
      'code' => 'Home',
      'parent_id' => '0',
      'sort' => '100',
      'value1' => 'Page',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '
',
    ),
    3201 =>
    array(
      'id' => '3201',
      'label' => 'About Us',
      'code' => 'BlobIntro',
      'parent_id' => '3200',
      'sort' => '101',
      'value1' => 'Text',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => 'LVH Global holds the highest standard of excellence in luxury vacation homes experiences.',
      'description' => 'LVH Global holds the highest standard of excellence in luxury vacation homes experiences. Each of our homes offers its own uniquely magnificent experience created to exceed your expectations  with unrivalled amenities and unparalleled service. With over 5000 full service 5-star luxury homes in over 50 destinations worldwide, endless possibilities await you. 
',
    ),
    3202 =>
    array(
      'id' => '3202',
      'label' => 'Complimentary Services',
      'code' => 'BlobCompServices',
      'parent_id' => '3200',
      'sort' => '102',
      'value1' => 'HeaderJSON',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' =>
      array(
        0 =>
        array(
          0 => 'Concierge Team',
          1 => 'Customize experience to your taste and available for any needs.',
        ),
        1 =>
        array(
          0 => 'Local Manager',
          1 => 'Manage the home, staff and provisioning throughout your stay.',
        ),
        2 =>
        array(
          0 => 'Butler',
          1 => 'Available for your service and planning needs.',
        ),
        3 =>
        array(
          0 => 'Maid',
          1 => 'Full time maid using only organic products.',
        ),
        4 =>
        array(
          0 => 'Chauffeur',
          1 => 'To and from the airport. Available for addtional charge as needed.',
        ),
        5 =>
        array(
          0 => 'Warm Welcome',
          1 => 'Champagne, hors d\'oeuvres and fresh floral arrangement.',
        ),
        6 =>
        array(
          0 => 'Luxury Amenities',
          1 => 'In room safe, linens, spa prodcuts, hairdryers & espresso service.',
        ),
        7 =>
        array(
          0 => 'Other Services',
          1 => 'Any staffing, in home services, car and yachts as needed at additional charge.',
        ),
      ),
    ),
    3204 =>
    array(
      'id' => '3204',
      'label' => 'Image Prive',
      'code' => 'ImagePrive',
      'parent_id' => '3200',
      'sort' => '103',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Prive.jpg',
    ),
    3818 =>
    array(
      'id' => '3818',
      'label' => 'Image Complimentary Services',
      'code' => 'ImageComplimentary',
      'parent_id' => '3200',
      'sort' => '103',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'About_Us.png',
    ),
    3820 =>
    array(
      'id' => '3820',
      'label' => 'Image About Us',
      'code' => 'ImageAboutUs',
      'parent_id' => '3200',
      'sort' => '103',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'Hero.jpg',
    ),
    3205 =>
    array(
      'id' => '3205',
      'label' => 'LVH Prive',
      'code' => 'BlobPriveIntro',
      'parent_id' => '3200',
      'sort' => '104',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => 'With your booking you will receive access to LVH Prive our exclusive lifestyle community of elite homeowners, travelers and tastemakers. ',
      'description' => 'With your booking you will receive access to LVH Prive our exclusive lifestyle community of elite homeowners, travelers and tastemakers.  LVH PRIVE will provide access to most exclusive ultimate travel experiences by providing our clients something they can\'t find anywhere with unrestricted access to.',
    ),
    3819 =>
    array(
      'id' => '3819',
      'label' => 'Complimentary Intro',
      'code' => 'BlobComplimentaryIntro',
      'parent_id' => '3200',
      'sort' => '104',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => 'Complimentary Services. Complimentary Services. Complimentary Services. Complimentary Services. Complimentary Services. Complimentary Services.',
      'description' => 'Complimentary Services. Complimentary Services. Complimentary Services. Complimentary Services. Complimentary Services. Complimentary Services. Complimentary Services.Complimentary Services. Complimentary Services. Complimentary Services. Complimentary Services. Complimentary Services.',
    ),
    3596 =>
    array(
      'id' => '3596',
      'label' => 'Prive Services List',
      'code' => 'BlobPriveServices',
      'parent_id' => '3200',
      'sort' => '105',
      'value1' => 'JSON',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' =>
      array(
        0 => 'Access to our private collection via the mobile application.',
        1 => 'A dedicated concierge team who will handle all of your inquiries.',
        2 => 'Last minute special promotions up to 50% discount.',
        3 => 'Exclusive access to sponsorship events and activation experiences.',
        4 => 'Dedicated loyalty and referrals program.',
        5 => 'Exclusive packages and experiences.',
      ),
    ),
    3252 =>
    array(
      'id' => '3252',
      'label' => 'Packages',
      'code' => 'Packages',
      'parent_id' => '0',
      'sort' => '127',
      'value1' => 'Page',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => '
',
    ),
    3253 =>
    array(
      'id' => '3253',
      'label' => 'Image Packages',
      'code' => 'ImagePackages',
      'parent_id' => '3252',
      'sort' => '127',
      'value1' => 'Image',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'About_Us.png',
    ),
    3254 =>
    array(
      'id' => '3254',
      'label' => 'Packages',
      'code' => 'BlobPackagesIntro',
      'parent_id' => '3252',
      'sort' => '127',
      'value1' => 'HeaderText',
      'value2' => NULL,
      'value3' => NULL,
      'value4' => NULL,
      'description' => 'LVH goes above and beyond to personalize your vacation and to create the dream vacation experience you deserve. We work with each client to create a unique and customized experience for you and your guests.
',
    ),
  ),
  'indexes' =>
  array(
    'code' =>
    array(
      'Constants' => '3628',
      'AddressStreet' => '3629',
      'CityStateCountry' => '3630',
      'Website' => '3631',
      'PhoneMain' => '3633',
      'EmailBooking' => '3634',
      'SocialFacebook' => '3635',
      'SocialInstagram' => '3636',
      'SocialLinkedIn' => '3637',
      'SocialYoutube' => '3638',
      'VideoCompany' => '3671',
      'RegionOrder' => '3971',
      'Services' => '3207',
      'ImageHomeIntro' => '3208',
      'BlobHomesIntro' => '3209',
      'BlobHomesDetail' => '3210',
      'ImageServicesIntro' => '3211',
      'BlobServicesIntro' => '3212',
      'BlobHomeownerIntro' => '3222',
      'ImagePartnerIntro' => '3215',
      'BlobPartnerIntro' => '3227',
      'ImageAffiliatesIntro' => '3217',
      'BlobAffiliatesIntro' => '3218',
      'BlobSellingPoints' => '3219',
      'ServicesHomeowner' => '3220',
      'ImageHomeowner' => '3221',
      'BlobHomeownerBenefits' => '3224',
      'BlobHomeownerCTA' => '3225',
      'ServicesPartner' => '3226',
      'BlobPartnerProgram' => '3228',
      'BlobPartnerBenefits' => '3229',
      'ServiceEvents' => '3230',
      'ImageEvents' => '3231',
      'BlobEventsIntro' => '3232',
      'BlobEventsSpecialities' => '3233',
      'ImageEventsGallery' => '3234',
      'BlobEventsCTA' => '3236',
      'ServiceConcierge' => '3237',
      'ImageConcierge' => '3238',
      'BlogConciergeIntro' => '3239',
      'ImageConciergeExperience' => '3240',
      'BlobConciergeExperience' => '3241',
      'BlobConciergeExperienceList' => '3242',
      'ImageConciergeSports' => '3243',
      'BlobConciergeSports' => '3244',
      'BlobConciergeSportsList' => '3245',
      'ImageConciergeSpa' => '3246',
      'BlobConciergeSpa' => '3247',
      'BlobConciergeSpaList' => '3248',
      'ImageConciergeStaff' => '3249',
      'BlobConciergeStaff' => '3250',
      'BlobConciergeStaffList' => '3251',
      'DestinationExperiences' => '3255',
      'ImageExperiences' => '3256',
      'BlobExperiencesIntro' => '3257',
      'Destination' => '3258',
      'CompanyTagline' => '3821',
      'CompanyName' => '3822',
      'PageTitlePreamble' => '3823',
      'CompanyCopyright' => '3824',
      'CompanyPrivacyPolicy' => '3825',
      'Home' => '3200',
      'BlobIntro' => '3201',
      'BlobCompServices' => '3202',
      'ImagePrive' => '3204',
      'ImageComplimentary' => '3818',
      'ImageAboutUs' => '3820',
      'BlobPriveIntro' => '3205',
      'BlobComplimentaryIntro' => '3819',
      'BlobPriveServices' => '3596',
      'Packages' => '3252',
      'ImagePackages' => '3253',
      'BlobPackagesIntro' => '3254',
    ),
  ),
);
