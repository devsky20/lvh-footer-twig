<?php

/**
 * Region
 */
$region = array(
  'data' =>
  array(
    315 =>
    array(
      'id' => '315',
      'label' => 'Asia & Africa',
      'code' => 'AAA',
      'parent_id' => '0',
      'country_id' => '0',
      'tax_rate' => '0.000',
      'service_rate' => '0.000',
      'currency' => '0',
      'lat' => '27.19555800',
      'lng' => '67.65887620',
      'map_zoom' => '2.0',
      'active_rent' => '1',
      'active_sale' => '0',
      'property_id' => NULL,
      'description' => '',
      'meta_keywords' => '',
      'meta_description' => '',
    ),
    314 =>
    array(
      'id' => '314',
      'label' => 'Caribbean',
      'code' => 'CAR',
      'parent_id' => '0',
      'country_id' => '0',
      'tax_rate' => '0.000',
      'service_rate' => '0.000',
      'currency' => '0',
      'lat' => '20.14539500',
      'lng' => '-68.36751800',
      'map_zoom' => '2.0',
      'active_rent' => '1',
      'active_sale' => '0',
      'property_id' => NULL,
      'description' => '',
      'meta_keywords' => '',
      'meta_description' => '',
    ),
    313 =>
    array(
      'id' => '313',
      'label' => 'Europe',
      'code' => 'EUR',
      'parent_id' => '0',
      'country_id' => '0',
      'tax_rate' => '0.000',
      'service_rate' => '0.000',
      'currency' => '0',
      'lat' => '52.26815737',
      'lng' => '13.35937500',
      'map_zoom' => '2.0',
      'active_rent' => '1',
      'active_sale' => '1',
      'property_id' => NULL,
      'description' => '',
      'meta_keywords' => '',
      'meta_description' => '',
    ),
    488 =>
    array(
      'id' => '488',
      'label' => 'Global',
      'code' => 'GLOBAL',
      'parent_id' => '0',
      'country_id' => '0',
      'tax_rate' => '0.000',
      'service_rate' => '0.000',
      'currency' => '0',
      'lat' => '27.19555800',
      'lng' => '67.65887620',
      'map_zoom' => '2.0',
      'active_rent' => '1',
      'active_sale' => '0',
      'property_id' => NULL,
      'description' => '',
      'meta_keywords' => '',
      'meta_description' => '',
    ),
    360 =>
    array(
      'id' => '360',
      'label' => 'Other',
      'code' => 'OTHER',
      'parent_id' => NULL,
      'country_id' => NULL,
      'tax_rate' => NULL,
      'service_rate' => NULL,
      'currency' => NULL,
      'lat' => NULL,
      'lng' => NULL,
      'map_zoom' => NULL,
      'active_rent' => '0',
      'active_sale' => '0',
      'property_id' => NULL,
      'description' => NULL,
      'meta_keywords' => NULL,
      'meta_description' => NULL,
    ),
    312 =>
    array(
      'id' => '312',
      'label' => 'South America',
      'code' => 'SOA',
      'parent_id' => '0',
      'country_id' => '0',
      'tax_rate' => '0.000',
      'service_rate' => '0.000',
      'currency' => '0',
      'lat' => '16.63619188',
      'lng' => '-94.21875000',
      'map_zoom' => '2.0',
      'active_rent' => '1',
      'active_sale' => '0',
      'property_id' => NULL,
      'description' => '',
      'meta_keywords' => '',
      'meta_description' => '',
    ),
    311 =>
    array(
      'id' => '311',
      'label' => 'United States',
      'code' => 'USA',
      'parent_id' => '0',
      'country_id' => '0',
      'tax_rate' => '0.000',
      'service_rate' => '0.000',
      'currency' => '0',
      'lat' => '39.90973623',
      'lng' => '-95.97656250',
      'map_zoom' => '2.0',
      'active_rent' => '1',
      'active_sale' => '1',
      'property_id' => NULL,
      'description' => '',
      'meta_keywords' => '',
      'meta_description' => '',
    ),
  ),
  'indexes' =>
  array(
    'code' =>
    array(
      'AAA' => '315',
      'CAR' => '314',
      'EUR' => '313',
      'GLOBAL' => '488',
      'OTHER' => '360',
      'SOA' => '312',
      'USA' => '311',
    ),
    'parent_id' =>
    array(
      0 =>
      array(
        0 => '315',
        1 => '314',
        2 => '313',
        3 => '488',
        4 => '312',
        5 => '311',
      ),
      '' =>
      array(
        0 => '360',
      ),
    ),
    'country_id' =>
    array(
      0 =>
      array(
        0 => '315',
        1 => '314',
        2 => '313',
        3 => '488',
        4 => '312',
        5 => '311',
      ),
      '' =>
      array(
        0 => '360',
      ),
    ),
    'active_rent' =>
    array(
      1 =>
      array(
        0 => '315',
        1 => '314',
        2 => '313',
        3 => '488',
        4 => '312',
        5 => '311',
      ),
      0 =>
      array(
        0 => '360',
      ),
    ),
    'active_sale' =>
    array(
      0 =>
      array(
        0 => '315',
        1 => '314',
        2 => '488',
        3 => '360',
        4 => '312',
      ),
      1 =>
      array(
        0 => '313',
        1 => '311',
      ),
    ),
  ),
);
